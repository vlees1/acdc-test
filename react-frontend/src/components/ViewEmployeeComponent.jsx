import React, {Component} from "react";
import EmployeeService from "../services/EmployeeService";
import { withRouter } from "./withRouter";

class ViewEmployeeComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // id: this.props.match.params.id,
            id: this.props.params.id,
            employee: {}
        }
    }

    componentDidMount() {
        EmployeeService.getEmployeeById(this.state.id).then(res => {
            this.setState({employee: res.data});
        })
    }

    cancel(){
        //this.props.history.push('/employees');
        this.props.navigate('/employees');
    }

    render() {
        return (
            <div>
                <br></br>
                <button className="btn btn-secondary" onClick={this.cancel.bind(this)} style={{marginBottom: "10px"}}>Back</button>
                <br></br>
                <div className="card col-md-6 ofset-md-3">
                    <h3 className="text-center">View Employee Details</h3>
                    <div className="card-body">
                        <div className="row">
                            <label>Employee First Name:&nbsp;</label>
                            <div>{this.state.employee.firstName}</div>
                        </div>
                        <div className="row">
                            <label>Employee Last Name:&nbsp;</label>
                            <div>{this.state.employee.lastName}</div>
                        </div>
                        <div className="row">
                            <label>Employee Email ID:&nbsp;</label>
                            <div>{this.state.employee.emailID}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(ViewEmployeeComponent);
