import React, { Component } from 'react'
import EmployeeService from '../services/EmployeeService';
import { FormErrors } from './FormErrors';
import { withRouter } from "./withRouter";
import validator from 'validator';

class CreateEmployeeComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            //id: this.props.match.params.id,
            id: this.props.params.id,
            firstName: '',
            lastName: '',
            emailId: '',
            formErrors: {emailId: ''},
            emailValid: true,
            formValid: false
        }
        this.changeFirstNameHandler = this.changeFirstNameHandler.bind(this);
        this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
        this.saveOrUpdateEmployee = this.saveOrUpdateEmployee.bind(this);
    }

    // step 3
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            EmployeeService.getEmployeeById(this.state.id).then( (res) =>{
                let employee = res.data;
                this.setState({firstName: employee.firstName,
                    lastName: employee.lastName,
                    emailId : employee.emailId
                });
            });
        }        
    }

    validateField(value) {
      let fieldValidationErrors = this.state.formErrors;
      let emailValid = this.state.emailValid;
      console.log("value: " + value);
      emailValid = validator.isEmail(value);
      fieldValidationErrors.email = emailValid ? '' : ' is invalid';
      
      this.setState({formErrors: fieldValidationErrors,
                      emailValid: emailValid
                    }, this.validateForm);
    }
      
    validateForm() {
      this.setState({formValid: this.state.emailValid});
    }
    errorClass() {
      return(this.state.emailValid ? '' : ' is-invalid');
    }

    saveOrUpdateEmployee = (e) => {
        e.preventDefault();
        let employee = {firstName: this.state.firstName, lastName: this.state.lastName, emailId: this.state.emailId};
        
        // step 5
        if(this.state.id === '_add' && this.state.emailValid){
            EmployeeService.createEmployee(employee).then(res =>{
                //this.props.history.push('/employees');
                this.props.navigate('/employees');
            });
        }else if (this.state.emailValid){
            EmployeeService.updateEmployee(employee, this.state.id).then( res => {
                //this.props.history.push('/employees');
                this.props.navigate('/employees');
            });
        }
    }
    
    changeFirstNameHandler= (event) => {
        this.setState({firstName: event.target.value});
    }

    changeLastNameHandler= (event) => {
        this.setState({lastName: event.target.value});
    }

    changeEmailHandler= (event) => {
        this.setState({emailId: event.target.value});
        this.validateField(event.target.value);
    }

    cancel(){
        //this.props.history.push('/employees');
        this.props.navigate('/employees');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Employee</h3>
        }else{
            return <h3 className="text-center">Update Employee</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className='panel panel-default'>
                                    <FormErrors formErrors={this.state.formErrors} />
                                </div>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> First Name: </label>
                                            <input placeholder="First Name" name="firstName" className="form-control" 
                                                value={this.state.firstName} onChange={this.changeFirstNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Last Name: </label>
                                            <input placeholder="Last Name" name="lastName" className="form-control" 
                                                value={this.state.lastName} onChange={this.changeLastNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Email Id: </label>
                                            <input placeholder="Email Address" name="emailId" className={`form-control ${this.errorClass()}` }
                                                value={this.state.emailId} onChange={this.changeEmailHandler}/>
                                                <div className="invalid-feedback">Please enter valid email in email@domain.site</div>
                                        </div>

                                        <button className="btn btn-success" disabled={!this.state.emailValid} onClick={this.saveOrUpdateEmployee}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default withRouter(CreateEmployeeComponent);